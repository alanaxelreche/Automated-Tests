//RUN TEST = npx mocha --no-timeouts tests\Benza\home.js

const { Builder, By, until } = require("selenium-webdriver");
const should = require("chai").should();
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const chrome = require("selenium-webdriver/chrome");

// Dirección del ChromeDriver (tiene que ser la misma versión que la instalada en tu navegador)
const chromedriverPath = "webDrivers/chromedriver.exe";

describe("Verificar que la landingpage funciona como debe", function () {
  it("Verificando: Home", async function () {
    // Configuración de opciones para el navegador (incognito, no cache/plugins/extensiones, idioma)
    const options = new chrome.Options().addArguments(
      "--incognito",
      "--disable-cache",
      "--disable-plugins",
      "--disable-extensions",
      "--lang=es"
      // "--lang=en"
      // "--lang=pt"
    );

    //Exportamos "es.json" con todos los strings visibles en la landing
    const lg = require("../Lenguajes/es.json");

    // Correr el navegador con todas las "options" especificadas
    const driver = new Builder()
      .forBrowser("chrome")
      .setChromeOptions(options)
      .setChromeService(new chrome.ServiceBuilder(chromedriverPath))
      .build();
    let ulr_Landing = "https://test.benzavalue.com/";

    driver.manage().window().maximize();
    // const landingPage = await driver.getWindowHandle();

    await driver.manage().setTimeouts({ implicit: 5000 });

    await driver.get(ulr_Landing);

    await driver.executeScript(
      "window.scrollTo(0, document.body.scrollHeight);"
    );

    await driver.manage().setTimeouts({ implicit: 5000 });

    // ----- Corroborar títulos y textos de la landingpage ----- //
    // Hero section:
    let hero_youtube = await driver.wait(
      until.elementIsVisible(
        driver.findElement(
          By.xpath('//*[@id="hero"]/div/div/div[2]/div/div/iframe')
        )
      )
    );

    let hero_h1 = await driver
      .findElement(By.xpath('//*[@id="hero"]/div/div/div[1]/h1'))
      .getText();

    hero_h1.should.equal(lg["Digital Revenue Optimization"]);
    expect(hero_youtube).to.exist;

    // Section 1:
    let section1_img = await driver.wait(
      until.elementIsVisible(
        driver.findElement(
          By.xpath('//*[@id="main"]/section[1]/div/div/div[1]/img')
        )
      )
    );

    let section1_h2 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[1]/div/div/div[2]/h2'))
      .getText();

    let section1_h4 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[1]/div/div/div[2]/h4'))
      .getText();

    let section1_p1 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[1]/div/div/div[2]/p[1]'))
      .getText();

    let section1_p2 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[1]/div/div/div[2]/p[2]'))
      .getText();

    expect(section1_img).to.exist;
    section1_h2.should.equal(lg["How it works"]);
    section1_h4.should.equal(
      lg[
        "Benza is a software platform based on Artificial Intelligence that allows, in a simple and convenient way, to optimize the content, products and services consumption in your sites."
      ]
    );
    section1_p1.should.equal(
      lg[
        "To do this, we developed a value proposition based on unique personalization algorithms, generating higher sales, conversions and organic content consumption, improving the return of investment in customer acquisition."
      ]
    );
    section1_p2.should.equal(
      lg[
        "Benza optimizes in real time the user experience automatically, through AI and machine learning that empower site personalization for each client individually. Platform efficiency increases constantly, producing better results every time."
      ]
    );

    //Section 2
    let section2_h2 = await driver
      .wait(
        until.elementIsVisible(
          driver.findElement(
            By.xpath('//*[@id="main"]/section[2]/div/div/div[1]/h2')
          )
        )
      )
      .getText();

    let section2_h4 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[2]/div/div/div[1]/h4'))
      .getText();

    let section2_p1 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[2]/div/div/div[1]/p[1]'))
      .getText();

    let section2_p2 = await driver
      .findElement(By.xpath('//*[@id="main"]/section[2]/div/div/div[1]/p[2]'))
      .getText();

    let section2_img = await driver.findElement(
      By.xpath('//*[@id="main"]/section[2]/div/div/div[2]/img')
    );

    section2_h2.should.equal(lg["How to implement Benza"]);
    section2_h4.should.equal(
      lg["You just need to implement a 2-line Javascript code on your html"]
    );

    section2_p1.should.equal(
      lg[
        "Thanks to a super simple implementation that takes place in a matter of minutes, it is not necessary to have IT dedicated resources (for technical implementation and support), being able to simply focus on your business development, without needing any integration with existing systems and platforms."
      ]
    );

    section2_p2.should.equal(
      lg[
        "You don’t need to modify your site, or add a widget or any special space; we can work with your current design, customizing it."
      ]
    );
    expect(section2_img).to.exist;
    // //Section 3
    // let section3_img = await driver.wait(
    //   until.elementIsVisible(
    //     driver.findElement(
    //       By.xpath('//*[@id="main"]/section[3]/div/div/div[1]/img')
    //     )
    //   )
    // );

    // let section3_h2 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[3]/div/div/div[2]/h2'))
    //   .getText();

    // let section3_h4 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[3]/div/div/div[2]/h4'))
    //   .getText();

    // let section3_p1 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[3]/div/div/div[2]/p[1]'))
    //   .getText();

    // let section3_p2 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[3]/div/div/div[2]/p[2]'))
    //   .getText();

    // let section3_p3 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[3]/div/div/div[2]/p[3]'))
    //   .getText();

    // section3_h2.should.equal(data.home.section3.h2);
    // section3_h4.should.equal(data.home.section3.h4);
    // section3_p1.should.equal(data.home.section3.p1);
    // section3_p2.should.equal(data.home.section3.p2);
    // section3_p3.should.equal(data.home.section3.p3);
    // expect(section3_img).to.exist;

    // //Section 4
    // let section4_img = await driver.wait(
    //   until.elementIsVisible(
    //     driver.findElement(
    //       By.xpath('//*[@id="main"]/section[4]/div/div/div[2]/img')
    //     )
    //   )
    // );

    // let section4_h2 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[4]/div/div/div[1]/h2'))
    //   .getText();

    // let section4_h4 = await driver
    //   .findElement(By.xpath('//*[@id="main"]/section[4]/div/div/div[1]/h4'))
    //   .getText();

    // section4_h2.should.equal(data.home.section4.h2);
    // section4_h4.should.equal(data.home.section4.h4);
    // expect(section4_img).to.exist;

    // //Footer
    // let footer_img = await driver.wait(
    //   until.elementIsVisible(
    //     driver.findElement(
    //       By.xpath('//*[@id="footer"]/div[1]/div/div/div[1]/a/img')
    //     )
    //   )
    // );

    // let footer_p = await driver
    //   .findElement(By.xpath('//*[@id="footer"]/div[1]/div/div/div[1]/p'))
    //   .getText();

    // let footer_h4_1 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[1]/h4')
    //   )
    //   .getText();

    // let footer_li_1 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[1]/ul/li[1]')
    //   )
    //   .getText();

    // let footer_li_2 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[1]/ul/li[2]')
    //   )
    //   .getText();

    // let footer_li_3 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[1]/ul/li[3]')
    //   )
    //   .getText();

    // let footer_li_4 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[1]/ul/li[4]')
    //   )
    //   .getText();

    // expect(footer_img).to.exist;
    // footer_p.should.equal(data.home.footer.p);
    // footer_h4_1.should.equal(data.home.footer.h4_1);
    // footer_li_1.should.equal(data.home.footer.li_1);
    // footer_li_2.should.equal(data.home.footer.li_2);
    // footer_li_3.should.equal(data.home.footer.li_3);
    // footer_li_4.should.equal(data.home.footer.li_4);

    // let footer_h4_2 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[2]/h4')
    //   )
    //   .getText();

    // let footer_li_5 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[2]/ul/li[1]')
    //   )
    //   .getText();

    // let footer_li_6 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[2]/ul/li[2]')
    //   )
    //   .getText();

    // let footer_li_7 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[2]/ul/li[3]')
    //   )
    //   .getText();

    // let footer_li_8 = await driver
    //   .findElement(
    //     By.xpath('//*[@id="footer"]/div[1]/div/div/div[2]/div/div[2]/ul/li[4]')
    //   )
    //   .getText();

    // footer_h4_2.should.equal(data.home.footer.h4_2);
    // footer_li_5.should.equal(data.home.footer.li_5);
    // footer_li_6.should.equal(data.home.footer.li_6);
    // footer_li_7.should.equal(data.home.footer.li_7);
    // footer_li_8.should.equal(data.home.footer.li_8);

    // let copyrightNotice = await driver
    //   .findElement(By.xpath('//*[@id="footer"]/div[2]/div'))
    //   .getText();

    // copyrightNotice.should.equal(data.home.footer.copyright);

    // await driver.quit();
  });
});
